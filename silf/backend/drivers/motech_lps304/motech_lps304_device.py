from silf.backend.drivers.motech_lps304.motech_lps304 import LPS304Driver, MockDriver
from silf.backend.commons.util.config import validate_mandatory_config_keys, open_configfile

class LPS304Device():
    device_id = 'Motech'
    live = False

    def init(self, config):
        print(config)
        device_id = self.device_id
        mandatory_config = ['filepath', 'timeout']
        self.config = open_configfile(config)

        if self.config is None:
            raise AssertionError("Config file required")

        print('', self.config, device_id, mandatory_config)
        validate_mandatory_config_keys(self.config, device_id, mandatory_config)

        if self.config.getboolean(device_id, 'use_mock', fallback=False):
            self.motech_driver = MockDriver()
        else:
            self.motech_driver = LPS304Driver(self.config[device_id]['filePath'],
                                               float(self.config[device_id]['timeout']))

    def power_up(self):
        if not self.motech_driver.is_open():
            self.motech_driver.open_port()

    def start_measurements(self):
        self.motech_driver.set_output(1)
        self.motech_driver.set_vdd(5)

    def set_v(self, v: float):
        self.motech_driver.set_v(v, 1)

    def get_v(self) -> float:
        return self.motech_driver.get_v()

    def power_down(self):
        self.stop_measurements()
        self.motech_driver.close_port()

    def stop_measurements(self):
        self.motech_driver.set_v(0, 1)
        self.motech_driver.set_output(0)
        self.motech_driver.set_vdd(0)
