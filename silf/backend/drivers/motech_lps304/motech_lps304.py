from silf.backend.commons.io.serialbase import SerialBase
import sys
from time import sleep


class LPS304Driver(SerialBase):
    def __init__(self, port: str, timeout: float=3):
        """
        :param port: Serial port file name (e.g. /dev/ttyUSB0)
        :param timeout: Serial port read timeout:
            None: wait forever
            0: non-blocking mode, return immediately in any case, 
               returning zero or more, up to the requested number of bytes
            x: set timeout to x seconds (float allowed) returns immediately
               when the requested number of bytes are available,
               otherwise wait until the timeout expires and return all bytes that
               were received until then.
        """
        super().__init__(port=port, baud=2400, databits=8, stopbits=1, parity='N', timeout=timeout)
        self.n_of_retries = 10
        self.open_port()

    def send_bytes(self, s: str ) -> int:
        """
        Encode string to ASCII and send it to serial
        :return: number of bytes send
        """
        return self.tty.write(s.encode("ASCII"))

    def if_ok(self) -> bool:
        """
        If command was successful several '\r\n' and 'OK' is returned
        :return: True or False
        """
        #Wait for confirmation
        for i in range(self.n_of_retries):
            sleep(0.2)
            if self.tty.in_waiting:
                break
        return "OK" in self.tty.read(self.tty.in_waiting).decode("ASCII")

    def set_v(self, val: float, channel: int=1) -> bool:
        """
        Set voltage
        :param val: voltage to set 
        :param channel: 1 for setting voltage on '+' channel; 2 for '-' channel
        :return: True on success
        """
        if val > 30:
            val =30
        if val < 0:
            val = 0
        self.send_bytes('vset{:d} {:.2f}\r'.format(channel, val))
        return self.if_ok()

    def get_v(self, channel: int=1) -> float:
        """
        Get voltage
        :param channel: 1 to get voltage from '+' channel; 2 from '-' channel
        :return: voltage
        """
        self.send_bytes('vout{:d}\r\n'.format(channel))
        #Wait for confirmation
        for i in range(self.n_of_retries):
            sleep(0.2)
            if self.tty.in_waiting:
                break
        ret = self.tty.read(self.tty.in_waiting).decode("ASCII")
        #Voltage is in string containing several '\r\n' and 'OK'
        if 'OK' in ret:
            for i in ret.split():
                try:
                    f = float(i)
                    return f
                except:
                    continue
        raise RuntimeError("Can not get voltage")

    def set_output(self, on: int) -> bool:
        """
        Set state of output
        :param on: 1 - output on; 0 - output off
        :return: True on success
        """
        if on > 0:
            on = 1
        else:
            on = 0
        self.send_bytes("out{:d}\r".format(on))
        return self.if_ok()

    def set_vdd(self, on: int) -> bool:
        """
        Set state of vdd (digital output)
        :param on: 0 - vdd off; 3 - vdd = 3.3V; 5 vdd = 5V
        :return: True on success
        """
        if on > 0 and on !=3 :
            on = 5
        if on < 0:
            on = 0
        self.send_bytes("vdd{:d}\r".format(on))
        return self.if_ok()

    def is_open(self) -> bool:
        """
        :return: If USB device is open
        """
        return self.tty and self.tty.isOpen()


class MockDriver():
    def __init__(self):
        self.v = 0

    def set_v(self, val: float, channel: int=1) -> bool:
        self.v = val

    def get_v(self, channel: int=1) -> float:
        return self.v

    def set_output(self, on: int) -> bool:
        return True

    def set_vdd(self, on: int) -> bool:
        return True

    def close_port(self):
        pass

    def is_open(self) -> bool:
        return True

# Test
if __name__ == "__main__":
    motech = LPS304Driver(sys.argv[1], 2)
    motech.set_v(1)
    print(motech.get_v())
    motech.set_output(1)
    motech.set_vdd(5)
    motech.close_port()




