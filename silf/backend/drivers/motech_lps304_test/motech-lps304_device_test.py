import unittest

from silf.backend.drivers.motech_lps304.motech_lps304_device import LPS304Device
from silf.backend.commons.util.config import prepend_current_dir_to_config_file


class TestMotechLPS304Device(unittest.TestCase):
    def test_device_working(self):
        self.device = LPS304Device()
        self.device.init(prepend_current_dir_to_config_file("motech_config.ini"))
        self.do_test()


    def test_mock_working(self):
        self.device = LPS304Device()
        self.device.init(prepend_current_dir_to_config_file("mock_config.ini"))
        self.do_test()

    def do_test(self):
        v = 4.52
        self.device.power_up()
        self.device.start_measurements()
        self.device.set_v(v)
        ret = self.device.get_v()
        self.assertAlmostEqual(v, ret, 1)
        self.device.stop_measurements()
        self.device.power_down()


if __name__ == '__main__':
    unittest.main()
